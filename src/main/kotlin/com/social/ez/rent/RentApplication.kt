package com.social.ez.rent

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RentApplication

fun main(args: Array<String>) {
	runApplication<RentApplication>(*args)
}
