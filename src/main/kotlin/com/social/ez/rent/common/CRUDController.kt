package com.social.ez.rent.common

import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface CRUDController<T : Model.Idable> {
    fun service(): CRUDService<T>

    @GetMapping("")
    fun get(): Flux<T> {
        return service().findAll()
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable id: String): Mono<T> {
        return service().findById(id)
    }

    @PostMapping("")
    fun save(@RequestBody model: T): Mono<T> {
        return service().save(model)
    }

    @DeleteMapping("")
    fun delete(id: String): Mono<Void> {
        return service().deleteById(id)
    }
}
