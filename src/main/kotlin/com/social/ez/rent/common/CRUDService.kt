package com.social.ez.rent.common

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface CRUDService<T : Model.Idable> {
    val repo: ReactiveCrudRepository<T, String>

    fun findAll(): Flux<T> = repo.findAll()

    fun findById(id: String): Mono<T> = repo.findById(id)

    fun save(m: T): Mono<T> = repo.save(m)


    fun deleteById(id: String): Mono<Void> = repo.deleteById(id)
}

