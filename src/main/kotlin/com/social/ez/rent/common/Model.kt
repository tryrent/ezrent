package com.social.ez.rent.common

object Model {
    interface Idable {
        val id: String?
    }
}
