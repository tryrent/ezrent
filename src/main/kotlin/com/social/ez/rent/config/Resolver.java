package com.social.ez.rent.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.WildcardType;

import java.util.Collection;

public class Resolver {
    public static AlternateTypeRule[] getRules(TypeResolver typeResolver){
        return new AlternateTypeRule[]{AlternateTypeRules.newRule(
                typeResolver.resolve(Flux.class, WildcardType.class),
                typeResolver.resolve(Collection.class, WildcardType.class)),
                AlternateTypeRules.newRule(
                        typeResolver.resolve(Flux.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(Collection.class, WildcardType.class)),
                AlternateTypeRules.newRule(
                        typeResolver.resolve(Mono.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(WildcardType.class)),
                AlternateTypeRules.newRule(
                        typeResolver.resolve(Mono.class, WildcardType.class),
                        typeResolver.resolve(WildcardType.class))};
    }
}
