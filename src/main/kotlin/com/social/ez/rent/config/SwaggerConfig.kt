package com.social.ez.rent.config

import com.fasterxml.classmate.TypeResolver
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux
import java.util.*


@Configuration
@EnableSwagger2WebFlux
class SwaggerConfig {

    @Bean
    fun api(typeResolver: TypeResolver): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .alternateTypeRules(*Resolver.getRules(typeResolver))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.social.ez.rent"))
                .paths(PathSelectors.any())
                .build()
                .produces(Collections.singleton(MediaType.APPLICATION_JSON_VALUE))
                .consumes(Collections.singleton(MediaType.APPLICATION_JSON_VALUE))
    }


    private val apiInfo by lazy {
        ApiInfoBuilder()
                .title("test")
                .description("This is an example microservice application.")
                .build()
    }
}
