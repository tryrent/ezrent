package com.social.ez.rent.user


import com.social.ez.rent.common.CRUDController
import com.social.ez.rent.common.CRUDService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/user")
class UserController(val userService: UserService) : CRUDController<User> {
    override fun service(): CRUDService<User> {
        return userService
    }
}
