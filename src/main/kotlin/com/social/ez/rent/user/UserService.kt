package com.social.ez.rent.user

import com.social.ez.rent.common.CRUDService
import org.springframework.stereotype.Service


@Service
class UserService(override val repo: UserRepository) : CRUDService<User>

