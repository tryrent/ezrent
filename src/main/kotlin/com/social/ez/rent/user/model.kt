package com.social.ez.rent.user

import com.fasterxml.jackson.annotation.JsonProperty
import com.social.ez.rent.common.Model
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import javax.validation.constraints.Email
import javax.validation.constraints.Size

@Document("user")
data class User(@Id override val id: String?,
                @field:Email
                @Indexed(unique = true)
                val email: String,
                @field:Size(min = 2)
                val firstName: String,
                @field:Size(min = 2)
                val lastName: String,
                @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
                val password: String?,
                val roleIds: List<String> = emptyList()) : Model.Idable
